Feature: Check Webpage displays correct elements
 I want to ensure the skynews.com website displays correctly

Background: User is on homepage
 Given I have navigated to the Sky News Home page
 When the page has loaded

 Scenario: Browser Tab Title Displays Correct Title (Test A)
 Then the browser tab title should display "The Latest News from the UK and Around the World | Sky News"

 Scenario: Correct New Categories should be displayed on homepage (Test B)
 Then all 15 categories should be available
 And the following <cateories> should be displayed
 Examples:
    | categories |
    | newsCategory | 
    | Home |
    | UK |
    | World |
    | Politics |
    | US |
    | Climate |
    | Science & Tech |
    | Business |
    | Ents & Arts |
    | Travel |
    | Offbeat |


Scenario: Home Category is a the default focus when navigating to Sky New Webpage (Test C)
 Then the "Home" Category is the default focus category

Scenario: When Clicking a new news category it becomes the default focus (Test D)
 And the Home Category is the default focus category
 And I click on "Climate"
 Then the "Climate" Category is the default focus category

 Scenario: Tab Title matches Article Title (Test E)
 And I navigate to a new Article
 Then the browser tab title matches the article headline