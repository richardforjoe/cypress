/// <reference types="cypress" />

import {TodoPage} from '../page-object/todo-page'

describe('My First Test', () => {

    const todopage = new TodoPage();

    it('Gets, types and asserts', () => {
      
      todopage.navigate('https://example.cypress.io')
  
      cy.contains('type').click()
  
      // Should be on a new URL which includes '/commands/actions'
      cy.url().should('include', '/commands/actions')
  
      // Get an input, type into it and verify that the value has been updated
      cy.get('.action-email')
        .type('fake@email.com')
        .should('have.value', 'fake@email.com')
    })
  })