import { Given } from "cypress-cucumber-preprocessor/steps";
import homePage from "../../../support/page-object/HomePage"

const url = 'https://news.sky.com/uk'
Given('I open Sky page', () => {
  const home =  new homePage
  home.launch()
})