import 'cypress-iframe'

class HomePage
{

launch()
{
    const url = 'https://news.sky.com/uk'
    cy.clearCookies();
    cy.viewport('macbook-15');
    cy.visit(url)
    cy.acceptCookiePolicy();
}

}

export default HomePage;